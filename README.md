Welcome to Village on the Green Life is meant to be enjoyed. Finding a home to be enjoyed is as important as anything in life. Village on the Green will help you enjoy life with its fantastic amenities, quaint location, and spacious floor plans.

Address : 2975 Continental Colony Parkway SW, Atlanta, GA 30331

Phone : 404-344-9909